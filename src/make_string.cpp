
#include <fit.hpp>
#include <string>
#include <sstream>
#include <cassert>

using namespace fit;

// sample(introspection-stringify)
FIT_STATIC_LAMBDA_FUNCTION(stringify) = conditional(
    [](const auto& x) FIT_RETURNS(std::to_string(x)),
    [](const auto& x) FIT_RETURNS((std::ostringstream() << x).str())
);
// end-sample

struct custom
{
    int x;

    friend std::ostream& operator<<(std::ostream& s, const custom& x)
    {
        s << x.x;
        return s;
    }
};

int main() {

    int x = 1;
    assert(stringify(x) == "1");

    custom y;
    y.x = 1;
    assert(stringify(y) == "1");
}
