
#include <fit.hpp>
#include <tuple>
#include <cassert>

using namespace fit;

#if 1
// sample(sequence-tuple_transform)
FIT_STATIC_LAMBDA_FUNCTION(tuple_transform) = [](auto&& sequence, auto f)
{
    return unpack(by(f, construct<std::tuple>()))
        (std::forward<decltype(sequence)>(sequence));
};
// end-sample
#else
// sample(sequence-pointfree-tuple_transform)
FIT_STATIC_FUNCTION(by_tuple) = capture(construct<std::tuple>())(flip(by));

FIT_STATIC_FUNCTION(tuple_transform) = 
    flip(compress(apply, compose(unpack, by_tuple)));
// end-sample
#endif

// sample(sequence-tuple_for_each)
FIT_STATIC_LAMBDA_FUNCTION(tuple_for_each) = [](auto&& sequence, auto f)
{
    return unpack(by(f))(std::forward<decltype(sequence)>(sequence));
};
// end-sample
// sample(sequence-tuple_fold)
FIT_STATIC_LAMBDA_FUNCTION(tuple_fold) = [](auto&& sequence, auto f)
{
    return unpack(compress(f))(std::forward<decltype(sequence)>(sequence));
};
// end-sample

// sample(sequence-tuple_cat)
FIT_STATIC_FUNCTION(tuple_cat) = unpack(construct<std::tuple>());
// end-sample

// sample(sequence-tuple_join)
FIT_STATIC_FUNCTION(tuple_join) = unpack(tuple_cat);
// end-sample

// sample(sequence-monad)
FIT_STATIC_LAMBDA_FUNCTION(tuple_for) = compose(tuple_join, tuple_transform);

FIT_STATIC_FUNCTION(tuple_yield) = construct<std::tuple>();
// end-sample

// sample(sequence-tuple_yield_if)
FIT_STATIC_LAMBDA_FUNCTION(tuple_yield_if) = [](auto c) FIT_RETURNS
(
    conditional(
        if_(c)(construct<std::tuple>()),
        always(std::tuple<>())
    )
);
// end-sample

// sample(sequence-tuple_filter)
FIT_STATIC_LAMBDA_FUNCTION(tuple_filter) = [](auto&& sequence, auto predicate)
{
    return tuple_for(std::forward<decltype(sequence)>(sequence), [&](auto&& x)
    {
        return tuple_yield_if(predicate(std::forward<decltype(x)>(x)))
            (std::forward<decltype(x)>(x));
    });
};
// end-sample

// sample(sequence-tuple_zip_with)
FIT_STATIC_LAMBDA_FUNCTION(tuple_zip_with) = [](auto&& sequence1, auto&& sequence2, auto f)
{
    auto&& functions = tuple_transform(
        std::forward<decltype(sequence1)>(sequence1),
        [&](auto&& x)
        {
            return [&](auto&& y)
            {
                return f(std::forward<decltype(x)>(x), std::forward<decltype(y)>(y));
            };
        }
    );
    auto combined = unpack(capture(construct<std::tuple>())(combine))(functions);
    return unpack(combined)(std::forward<decltype(sequence2)>(sequence2));
};
// end-sample

void run_transform()
{
    // sample(sequence-run_transform)
    auto t = std::make_tuple(1, 2);
    auto r = tuple_transform(t, [](int i) { return i*i; });
    assert(r == std::make_tuple(1, 4));
    // end-sample
    (void)r;
}

void run_filter()
{
    // sample(sequence-run_filter)
    auto t = std::make_tuple(1, 2, 'x', 3);
    auto r = tuple_filter(t, [](auto x) { return std::is_same<int, decltype(x)>(); });
    assert(r == std::make_tuple(1, 2, 3));
    // end-sample
    (void)r;
}

void run_zip()
{
    // sample(sequence-run_zip)
    auto t1 = std::make_tuple(1, 2);
    auto t2 = std::make_tuple(3, 4);
    auto p = tuple_zip_with(t1, t2, [](auto x, auto y) { return x*y; });
    int r = tuple_fold(p, [](auto x, auto y) { return x+y; });
    assert(r == (1*3 + 4*2));
    // end-sample
    (void)r;
}

int main() 
{
    run_transform();
    run_filter();
    run_zip();
}