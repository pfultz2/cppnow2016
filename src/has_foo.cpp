#include <fit.hpp>
#include <string>
#include <type_traits>
#include <cassert>

using namespace fit;

// sample(introspection-has_foo)
FIT_STATIC_LAMBDA_FUNCTION(has_foo) = conditional(
    [](auto x) -> decltype(x.foo(), std::true_type{})
    {
        return {};
    },
    [](auto x) -> std::false_type { return {}; }
);
// end-sample

struct C
{
    void foo() {}
};

int main() {

    static_assert(decltype(has_foo(C{})){}, "");
    static_assert(!(decltype(has_foo(0)){}), "");
}
