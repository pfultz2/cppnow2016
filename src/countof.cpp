
#include <type_traits>

// From Jonathan Adamczewski

#if 0

// sample(dep-countof-problem)
template<typename T, std::size_t N>
constexpr std::size_t countof(T const (&)[N]) noexcept
{
  return N;
}

struct S
{
  int a[4];
};

void f(S* s)
{
  constexpr size_t s_a_count = countof(s->a); 
  int b[s_a_count]; 
  // ...
}
// end-sample

#else

// sample(dep-countof)
template<typename T, std::size_t N>
constexpr std::integral_constant<std::size_t, N> countof(const T (&)[N]) noexcept
{
  return {};
}

struct S
{
  int a[4];
};

void f(S* s)
{
  constexpr auto s_a_count = decltype(countof(s->a)){}; 
  int b[s_a_count]; 
  // ...
}
// end-sample

#endif

int main() {

}
