
#include <tuple>
#include <fit.hpp>
#include <cassert>

using namespace fit;

// sample(dep-modern_tuple)
template<class... Ts>
struct modern_tuple
: std::tuple<Ts...>
{
    using base = std::tuple<Ts...>;
    template<class... Xs>
    modern_tuple(Xs&&... xs) : base(std::forward<Xs>(xs)...)
    {}

    template<class IntegralConstant>
    auto operator[](IntegralConstant)
    {
        return std::get<IntegralConstant::value>(static_cast<base&>(*this));
    }
};
// end-sample

FIT_STATIC_FUNCTION(make_modern_tuple) = construct<modern_tuple>();


int main() {

    // sample(dep-modern_tuple-example)
    auto t = make_modern_tuple(1, 2, 3);
    auto x = t[std::integral_constant<int, 2>{}];
    // end-sample

    assert(x == 2);
}
