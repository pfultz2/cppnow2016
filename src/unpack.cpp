
#include <fit.hpp>
#include <tuple>
#include <cassert>

using namespace fit;

// sample(sequence-custom-struct)
struct point
{
    int x;
    int y;

    point(int x, int y) : x(x), y(y)
    {}
};

namespace fit {
    template<>
    struct unpack_sequence<point>
    {
        template<class F, class P>
        static auto apply(F&& f, P&& p) FIT_RETURNS
        (
            f(p.x, p.y)
        );
    };
}
// end-sample

int main() {

    // sample(sequence-unpack)
    auto check_equal = [](auto x, auto y) { return x == y; };
    assert(fit::unpack(check_equal)(std::make_tuple(1, 1)));
    assert(fit::unpack(check_equal)(fit::pack(1, 1)));
    // end-sample

    // sample(sequence-unpack-all)
    assert(fit::unpack(check_equal)(std::make_tuple(1), std::make_tuple(1)));
    // end-sample

    // sample(sequence-unpack-custom-struct)
    assert(fit::unpack(check_equal)(point(1, 1)));
    // end-sample

}

