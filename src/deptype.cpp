
#include <type_traits>

// sample(dep-make)
template<int N>
constexpr std::integral_constant<int, N> make()
{
    return {};
}
// end-sample

int main() {
    make<5>();
}
