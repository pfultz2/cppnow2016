#include <fit.hpp>
#include <vector>
#include <map>
#include <string>
#include <iostream>

using namespace fit;


// sample(introspection-find-adl)
// ADL Lookup for ranges
namespace adl {

using std::begin;
using std::end;

template<class R>
auto adl_begin(R&& r) FIT_RETURNS(begin(r));
template<class R>
auto adl_end(R&& r) FIT_RETURNS(end(r));
}
// end-sample

#if 0

// sample(introspection-find)
FIT_STATIC_LAMBDA_FUNCTION(find_iterator) = conditional(
    [](const std::string& s, const auto& x)
    {
        auto index = s.find(x);
        if (index == std::string::npos) return s.end();
        else return s.begin() + index;
    },
    [](const auto& r, const auto& x) FIT_RETURNS(r.find(x)),
    [](const auto& r, const auto& x)
    {
        using std::begin;
        using std::end;
        return std::find(begin(r), end(r), x);
    }
);
// end-sample

#elif 0

// sample(introspection-find2)
FIT_STATIC_LAMBDA_FUNCTION(find_iterator) = conditional(
    [](const std::string& s, const auto& x)
    {
        auto index = s.find(x);
        if (index == std::string::npos) return s.end();
        else return s.begin() + index;
    },
    [](const auto& r, const auto& x) FIT_RETURNS(r.find(x)),
    [](const auto& r, const auto& x) -> decltype(adl::adl_begin(r), adl::adl_end(r))
    {
        using std::begin;
        using std::end;
        return std::find(begin(r), end(r), x);
    }
);
// end-sample

#else

// sample(introspection-find-custom-points)
FIT_STATIC_LAMBDA_FUNCTION(find_iterator) = conditional(
    [](const auto& r, const auto& x) FIT_RETURNS(strong_find(r, x)),
    [](const std::string& s, const auto& x)
    {
        auto index = s.find(x);
        if (index == std::string::npos) return s.end();
        else return s.begin() + index;
    },
    [](const auto& r, const auto& x) FIT_RETURNS(r.find(x)),
    [](const auto& r, const auto& x) -> decltype(adl::adl_begin(r), adl::adl_end(r))
    {
        using std::begin;
        using std::end;
        return std::find(begin(r), end(r), x);
    },
    [](const auto& r, const auto& x) FIT_RETURNS(weak_find(r, x))
);
// end-sample

#endif

// Implement an infix `in` operator to check if a range contains an element
FIT_STATIC_LAMBDA_FUNCTION(in) = infix(
    [](const auto& x, const auto& r)
    {
        using std::end;
        return find_iterator(r, x) != end(r);
    }
);
// Negate version of `in`
FIT_STATIC_LAMBDA_FUNCTION(not_in) = infix(compose(not _, in));

int main()
{
    // Check if vector contains element
    std::vector<int> numbers = { 1, 2, 3, 4, 5 };
    if (5 <in> numbers) std::cout << "Yes" << std::endl;

    // Check if string contains element
    std::string s = "hello world";
    if ("hello" <in> s) std::cout << "Yes" << std::endl;

    // Check if map contains element
    std::map<int, std::string> number_map = {
        { 1, "1" },
        { 2, "2" },
        { 3, "3" },
        { 4, "4" }
    };

    if (4 <in> number_map) std::cout << "Yes" << std::endl;

    // Check if map doesn't contains element
    if (not (8 <in> numbers)) std::cout << "No" << std::endl;
    if (8 <not_in> numbers) std::cout << "No" << std::endl;

}
