#include <fit.hpp>
#include <iostream>
#include <type_traits>

using namespace fit;

// sample(pointfree-print)
FIT_STATIC_FUNCTION(simple_print) = FIT_LIFT(std::ref(std::cout) << _);

FIT_STATIC_FUNCTION(print) = by(simple_print);
// end-sample

// sample(pointfree-print_lines)
using newline_t = std::integral_constant<char, '\n'>;
FIT_STATIC_FUNCTION(print_lines) = by(capture(newline_t{})(flip(print)));
// end-sample

// sample(pointfree-max)
FIT_STATIC_FUNCTION(max) = compress(FIT_LIFT(std::max));
// end-sample


int main() 
{
    simple_print("Hello\n");

    // sample(pointfree-print-example)
    print("Hello", "World\n");
    // end-sample

    print_lines("Hello", "World");

    // sample(pointfree-max-example)
    auto n = max(1, 2, 4, 3); // Returns 4
    auto m = max(0.1, 0.2, 0.5, 0.4); // Returns 0.5
    // end-sample

    print_lines(n, m);
}
