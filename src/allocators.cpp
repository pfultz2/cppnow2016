#include <fit.hpp>
#include <vector>
#include <map>
#include <string>
#include <array>
#include <iostream>

using namespace fit;

struct blk { void* ptr; size_t length; };
// sample(introspection-helpers)
FIT_STATIC_LAMBDA_FUNCTION(try_deallocate) = conditional(
    [](auto&& a, blk b) FIT_RETURNS(a.deallocate(b)),
    always()
);

FIT_STATIC_LAMBDA_FUNCTION(owns) = [](auto&& a, blk b) FIT_RETURNS(a.owns(b));
// end-sample

// sample(introspection-fallback_allocator)
template<class P, class F>
struct fallback_allocator 
// end-sample
{
    P primary;
    F fallback;

    fallback_allocator(P p, F f) : primary(p), fallback(f)
    {}

    // sample(introspection-allocate)
    blk allocate(size_t n) 
    {
        auto r = primary.allocate(n);
        if (!r.ptr) r = fallback.allocate(n);
        return r;
    }
    // end-sample

    // sample(introspection-deallocate)
    template<class Block>
    auto deallocate(Block b) FIT_RETURNS
    (
        owns(primary, b) ? 
            try_deallocate(primary, b) :
            try_deallocate(fallback, b)
    );
    // end-sample

    // sample(introspection-owns)
    template<class Block>
    auto owns(Block b) FIT_RETURNS(owns(primary, b) || owns(fallback, b));
    // end-sample
};

struct mallocator
{
    blk allocate(size_t n)
    {
        blk r;
        r.ptr = malloc(n);
        r.length = n;
        return r;
    }

    void deallocate(blk b)
    {
        free(b.ptr);
    }
};

struct region
{
    char *b, *e, *p;

    region(blk block)
    {
        b = p = (char*)block.ptr;
        e = b + block.length;
    }

    blk allocate(size_t n)
    {
        blk r;
        if (e - p < n) r.ptr = nullptr;
        else
        {
            r.ptr = p;
            r.length = n;
            p += n;
        }
        return r;
    }

    bool owns(blk block)
    {
        return (char*)block.ptr > b and (char*)block.ptr < e;
    }
};

int main() {
    std::array<char, 512> buf;
    blk stack_block;
    stack_block.ptr = buf.data();
    stack_block.length = buf.size();
    fallback_allocator<region, mallocator> a{region(stack_block), mallocator{}};

    auto b = a.allocate(1024);

}
